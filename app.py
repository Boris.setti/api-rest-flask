from bson.objectid import ObjectId
from pymongo import MongoClient
from flask import Flask, render_template, redirect, url_for, request, jsonify
import db
app = Flask(__name__)


@app.route('/v1/')
def home():
    return('home doc')


@app.route('/v1/tools/', methods=['GET'])
def getAllVeille():
    veilleList = db.db.veille.find()
    data = veilleList
    dataListe = []
    for liste in data:
        key = liste['_id']
        title = liste['title']
        link = liste['link']
        author = liste['author']
        description = liste['description']
        tagList = liste['taglist']
        result = {
            'id': str(key),
            'titre': title,
            'description': description,
            'author': author,
            'link': link,
            'tagList': tagList
        }
        dataListe.append(result)
    return jsonify(dataListe)


@app.route('/v1/tools/', methods=['POST'])
def postVeille():
    result = request.form
    error = False
    error_message = []

    try:
        result['title']
        if result['title'] == '':
            error = True
            error_message.append('title missing')
    except:
        error = True
        error_message.append('title missing')

    try:
        result['link']
        if result['link'] == '':
            error = True
            error_message.append('link missing')
    except:
        error = True
        error_message.append('link missing')

    try:
        result['author']
        if result['author'] == '':
            error = True
            error_message.append('author missing')
    except:
        error = True
        error_message.append('author missing')

    try:
        result['description']
        description = result['description']
    except:
        description = ''

    try:
        result['tagList']
        tagList = result['tagList']
    except:
        tagList = ""

    if error == False:
        data = {
            'title': result['title'],
            'link': result['link'],
            'author': result['author'],
            'description': description,
            'taglist': tagList
        }

        db.db.veille.insert(data)
        return(jsonify({'message': 'succes'}))
    else:
        return(jsonify({'message': 'error', 'error_message': error_message}))


@app.route('/v1/tools/<string:post_id>', methods=['DELETE'])
def getoneveille(post_id):

    key = {'_id': ObjectId(post_id)}
    db.db.veille.delete_one(key)
    return(jsonify({'message': 'succes'}))

@app.route('/v1/tools/<string:post_id>', methods=['GET'])
def test(post_id):
    key = {'_id': ObjectId(post_id)}
    data = db.db.veille.find(key)

    for liste in data:
        result = {
            'id': str(liste['_id']),
            'titre': liste['title'],
            'description':  liste['description'],
            'author': liste['author'],
            'link': liste['link'],
            'tagList': liste['taglist']
        }
    return (jsonify(result))


# @app.route('/v1/tools/<string:post_id>',methods = ['GET'])
# def test(post_id):
#     print(post_id)
#     return post_id

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
